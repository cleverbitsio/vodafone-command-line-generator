package com.cleverbits.io.spark_examples

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql._
import com.databricks.spark.csv
import org.apache.spark.sql.types._
import org.apache.spark.sql.cassandra
import org.apache.spark.sql.cassandra._
import com.datastax.spark
import com.datastax.spark._
import com.datastax.spark.connector
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql
import com.datastax.spark.connector.cql._
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector.cql.CassandraConnector._

/*
mvn package
spark-submit --class "com.cleverbits.io.spark_examples.Vodafone" --master local /Users/terry/dev/scala//Users/terry/dev/spark-2.1.1-bin-hadoop2.7/bin/spark-submit --class "com.cleverbits.io.spark_examples.Vodafone" --master local /Users/terry/dev/scala/maven_example/target/spark-examples-0.0.1-SNAPSHOT.jar
/opt/spark-2.1.1-bin-hadoop2.7/bin/spark-submit --class "com.cleverbits.io.spark_examples.Vodafone" --master local target/spark-examples-0.0.1-SNAPSHOT.jar
*/
object Vodafone {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("CSVSparkApp")
      .setMaster("local[*]")
      //.set("spark.cassandra.connection.host", "localhost")
      .set("spark.cassandra.connection.host", "node.c.graceful-matter-161422.internal")
      .set("spark.cassandra.connection.port", "9042")
      .set("spark.debug.maxToStringFields", "100") //needed because of the large number of columns
      .set("spark.cassandra.input.consistency.level", "LOCAL_ONE")
      .set("spark.cassandra.output.consistency.level", "LOCAL_ONE")

    val sc = new SparkContext(conf)
    var sqlContext = new SQLContext(sc)
    
    //val table = sc.cassandraTable("vodafone", "test")
    val table = sc.cassandraTable("vodafone", "prod")
    println(table.count) //0
    
    //val pathToCSV = "vodafone-test.csv"
    val pathToCSV = "test_with_header_trimmed.csv"

    val customSchema = StructType(Array(
        StructField("r_p_msisdn_code", IntegerType, true),
        StructField("r_p_msc", StringType, true),
        StructField("r_p_imsi_code", IntegerType, true),
        StructField("zpcode", IntegerType, true),
        StructField("actual_volume", IntegerType, true),
        StructField("r_p_imei", StringType, true),
        StructField("call_dest", StringType, true),
        StructField("credit_clicks", IntegerType, true),
        StructField("o_p_number", StringType, true),
        StructField("ttcode", IntegerType, true),
        StructField("r_p_ms_class_mark", StringType, true),
        StructField("start_d_t", DateType, true),
        StructField("twcode", IntegerType, true),
        StructField("rated_clicks", IntegerType, true),
        StructField("sp_code", IntegerType, true),
        StructField("r_p_sim_id", IntegerType, true),
        StructField("facility_usage", StringType, true),
        StructField("aoc_amount", IntegerType, true),
        StructField("tzcode", IntegerType, true),
        StructField("r_p_cgi", StringType, true),
        StructField("r_p_np", StringType, true),
        StructField("action_code", StringType, true),
        StructField("tm_version", IntegerType, true),
        StructField("umcode", IntegerType, true),
        StructField("rated_volume", IntegerType, true),
        StructField("rtx_lfnr", IntegerType, true),
        StructField("sncode", IntegerType, true),
        StructField("trac_start_d_t", StringType, true),
        StructField("rtx_type", StringType, true),
        StructField("original_start_d_t", DateType, true),
        StructField("rtx_sqn", IntegerType, true),
        StructField("r_p_ton", StringType, true),
        StructField("gvcode", IntegerType, true),
        StructField("o_p_cgi", StringType, true),
        StructField("termination_ind", IntegerType, true),
        StructField("r_p_mccode", IntegerType, true),
        StructField("rtx_sub_lfnr", IntegerType, true),
        StructField("r_p_customer_id", IntegerType, true),
        StructField("r_p_mcind", IntegerType, true),
        StructField("bc", IntegerType, true),
        StructField("call_type", IntegerType, true),
        StructField("rated_flat_amount", IntegerType, true),
        StructField("bill_status", StringType, true),
        StructField("aoc_ind", StringType, true),
        StructField("gsm_pi", StringType, true),
        StructField("market_id", StringType, true),
        StructField("plcode", IntegerType, true),
        StructField("eccode", IntegerType, true),
        StructField("roamer_ind", StringType, true),
        StructField("rounded_volume", IntegerType, true),
        StructField("delete_after_bill", StringType, true),
        StructField("tmcode", IntegerType, true),
        StructField("r_p_imsi", IntegerType, true),
        StructField("vpn_ind", StringType, true),
        StructField("tariff_class", StringType, true),
        StructField("call_reference", IntegerType, true),
        StructField("cause_for_term", StringType, true),
        StructField("pstn_id", IntegerType, true),
        StructField("routing_category", StringType, true),
        StructField("rated_flat_amount_euro", DoubleType, true),
        StructField("cdr_indicator", StringType, true),
        StructField("r_p_number", StringType, true),
        StructField("visited_cc_code", IntegerType, true),
        StructField("data_volume", IntegerType, true),
        StructField("o_p_rgn", IntegerType, true),
        StructField("roam_rerating_ind", IntegerType, true),
        StructField("inputed_to_primary", StringType, true),
        StructField("primary_object_id", IntegerType, true),
        StructField("origin_zpcode", IntegerType, true),
        StructField("dialled_digits", StringType, true),
        StructField("circe_product", StringType, true),
        StructField("counter", StringType, true),
        StructField("bonus", StringType, true),
        StructField("wisp", StringType, true),
        StructField("hot_spot", StringType, true),
        StructField("authentication_type", StringType, true),
        StructField("charging_id", IntegerType, true),
        StructField("ggsn_address", StringType, true),
        StructField("bearer", IntegerType, true),
        StructField("bit_rate", IntegerType, true),
        StructField("qosnegotiated", StringType, true),
        StructField("promo_elab_date", DateType, true),
        StructField("primary_gn_sim_id", IntegerType, true),
        StructField("connected_sim_id_from_gn", IntegerType, true),
        StructField("number_pm_pulses", IntegerType, true),
        StructField("apn_ind", IntegerType, true),
        StructField("delay_call", StringType, true),
        StructField("rated_flat_amount_euro_tm", DoubleType, true)
      ))

    val df = sqlContext.read
                    .format("com.databricks.spark.csv")
                    .option("header","true")
                    .option("timestampFormat", "dd-MM-yyyy HH:mm:ss")
                    //.option("timestampFormat", "dd/MM/yyyy HH:mm")
                    .option("inferSchema","true") 
                    //.schema(customSchema)
                    .load(pathToCSV)
    println("Starting ...")
    df.rdd.cache()
    //df.rdd.foreach(println)
    println(df.printSchema)
    df.registerTempTable("vodafone")
    //sqlContext.sql("SELECT somedatetime, web FROM vodafone LIMIT 3").collect.foreach(println)
    sqlContext.sql(s"SELECT R_P_SIM_ID, START_D_T FROM vodafone LIMIT 3").collect.foreach(println)

    
    val resultset = sqlContext.sql("SELECT * FROM vodafone")
    import org.apache.spark.sql._
    //resultset.write.mode("Append").cassandraFormat("test", "vodafone").save()
    resultset.write.mode("Append").cassandraFormat("prod", "vodafone").save()

    println("... Finished")
    
  }
}
